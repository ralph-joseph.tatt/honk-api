from flask import *


app = Flask(__name__)

@app.route("/")
def welcome_screen():
    return "Welcome to honk-api, go to /honk or /loud_honk <br> <img src=\"https://ichef.bbci.co.uk/news/660/cpsprodpb/12B60/production/_109004667_02untitledgoosegamescreen3840x2160.png\">"

@app.errorhandler(404)
def resource_not_found(e):
    return "Honk? <br> (honk not found)"

@app.route("/honk")
def honk():
    return "honk"

@app.route("/loud_honk")
def loud_honk():
    return "<h1>HONK!</h1>"

if __name__ == "__main__":
    host = "0.0.0.0"
    port = 80
    app.run(host, port)
